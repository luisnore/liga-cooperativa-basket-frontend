import { useEffect, useRef } from 'react'
import { cn } from '../lib/utils'
import { CloseIcon } from './Icons'

export default function Modal({ isOpen, onClose, children, className }) {
  const modalRef = useRef(null)

  useEffect(() => {
    const modalElement = modalRef.current
    if (modalElement) {
      if (isOpen) {
        modalElement.showModal()
      } else if (modalElement.open) {
        modalElement.close()
      }
    }
  }, [isOpen])

  const handleCloseModal = () => {
    if (onClose) {
      onClose()
    }
  }

  const handleKeyDown = event => {
    if (event.key === 'Escape') {
      handleCloseModal()
    }
  }

  const handleClickOutside = event => {
    const modalElement = modalRef.current
    if (modalElement && event.target === modalElement) {
      handleCloseModal()
    }
  }

  return (
    <dialog
      ref={modalRef}
      onKeyDown={handleKeyDown}
      onClose={handleCloseModal}
      onClick={handleClickOutside}
      className={cn(
        'backdrop:bg-black backdrop:opacity-20 p-4 rounded-xl shadow-[2px_0_8px_0_rgba(0,0,0,0.1)] max-w-9/12 md:max-w-2xl',
        className,
      )}
    >
      <header className='text-right'>
        <button className='modal-close-btn' onClick={handleCloseModal}>
          <CloseIcon />
        </button>
      </header>
      {children}
    </dialog>
  )
}
