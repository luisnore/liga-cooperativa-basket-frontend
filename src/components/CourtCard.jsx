import React from 'react'
import LinkButton from '../components/UI/LinkButton'

const CourtCard = ({ court, user }) => {
  return (
    <div
      key={court.id}
      className='border-solid border-2 border-orange-500 rounded bg-white p-0'
    >
      <div className=' py-2 text-center bg-orange-100'>
        <h5 className='font-semibold'>{court.name}</h5>
      </div>
      <div className='pt-2 pb-4 px-2 text-center'>
        <strong className='font-semibold'>Dirección: </strong>
        <span className='mt-2'>{court.direction}</span>
      </div>
        {court.lights ? (
          <div className='pt-2 pb-4 px-2 text-center'>
            <strong className='font-semibold'>Luces: </strong>
            {court.lightTimeout}
          </div>
        ) : null}
      <div className='pt-2 pb-4 px-2 text-center'>
        <strong className='font-semibold'>Estado: </strong>
        <span className='mt-2'>{court.status}</span>
      </div>

      { user ?
      <div className='pt-2 pb-2 px-2 text-center'>
        <LinkButton
          url= {`/court/${court.id}/edit`}
          className='grid items-center bg-white'
          variant='default'
        >
          Editar
        </LinkButton>
      </div>
      : null}
    </div>
  )
}

export default CourtCard
