import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import { useUserContext } from '../context/UserContext'

export const PrivateRoute = () => {
  const { user } = useUserContext()
  if (!user) {
    return <Navigate to='/login' />
  }
  return <Outlet />
}

export const PrivateAdminRoute = () => {
  const { user } = useUserContext()
  if (!user?.admin) {
    return <Navigate to='/' />
  }
  return <Outlet />
}
