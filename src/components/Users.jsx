import React from 'react'
import {
  redirect, useLoaderData, useActionData
} from 'react-router-dom'
import { Md5 } from 'ts-md5'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'
import Form from '@/components/UI/Form/Form'
import LinkButton from '@/components/UI/LinkButton'
import Table from '@/components/UI/Table/Table'



export async function newUserAction({ request }) {
  let formData = await request.formData()
  const user = Object.fromEntries(formData)
  if (user.password != user.repeatPassword){
    return {error: 'Las contraseñas no coinciden'}
  }
  if (user.admin === 'on'){
    user.admin = true
  }
  user.password = Md5.hashStr(user.password)
  user.repeatPassword = Md5.hashStr(user.repeatPassword)
  await fetchData(
    `${apiUrl}${serverEndpoints.user}`,
    'POST',
    user
  )
  return redirect('/admin/newuser')
}

export async function actionLinkUserTeam({ request }){
  let formData = await request.formData()
  const data = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.user}${data.id}`,
    'PUT',
    data
  )
  return redirect('/admin')
}

export async function loaderNewUser() {
  let teams = await fetchData(`${apiUrl}${serverEndpoints.team}`, 'GET')
  let users = await fetchData(`${apiUrl}${serverEndpoints.user}` + '?expanded=true', 'GET')
  return { teams, users }
}

export async function loaderUsersTeams() {
  let users = await fetchData(`${apiUrl}${serverEndpoints.user}`, 'GET')
  let teams = await fetchData(`${apiUrl}${serverEndpoints.team}`, 'GET')
  return { users, teams }
}

export function CreateUserForm () {
  const data = useLoaderData()
  const error = useActionData()
  const htmlTeams = []
  for (let team of data.teams) {
    htmlTeams.push(<option key={team.id} value={team.id} >{team.name}</option>)
  }
  return (
    <>
      <Form >
        <h1 className='font-bold'>Cambiar contraseña</h1>
        <br/>
        <span>Nombre de Usuario</span>
        <Form.Input id='name' type='text' text='Usuario' />
        <br/>
        <span>Contraseña</span>
        <br/>
        <Form.Input id='password' type='password' text='Nueva Contraseña' />
        <br/>
        <span>Repetir Contraseña</span>
        <Form.Input
          id='repeatPassword'
          type='password'
          text='Repetir contraseña'
        />
        <br/>
        <span>Team</span>
        <Form.Select name="teamId" >
          <option key="default" >-- Select a Team --</option>
          {htmlTeams}
        </Form.Select>
        <br/>
        <span>App Admin</span>
        <Form.Input
          type="checkbox"
          id="admin"
          text="App Admin"
          value="False"
        />
        <br/>
        {error && (
          <p className='text-red-600'>{error.error}</p>
        )}
        <Form.Button type='submit' className='' variant='subtle'>
          Crear Usuario
        </Form.Button>
        <LinkButton
          url='/admin'
          text='Volver'
          type='submit'
          className="bg-white border-solid border-2 rounded mt-8"
        >
          Volver
        </LinkButton>
      </Form>
      <ListUsers/>
    </>
  )
}

export function ListUsers() {
  const data = useLoaderData()
  return(
    <Table>
      <Table.THead>
        <Table.TRow>
          <Table.THCol>Nombre</Table.THCol>
          <Table.THCol>Equipo</Table.THCol>
          <Table.THCol>Admin</Table.THCol>
        </Table.TRow>
      </Table.THead>
      <tbody>
        {data.users.length === 0 ? (
          <p className=''>
           No hay equipos
          </p>
        ) : (
          data.users.map((user,i) =>
            <Table.TRow key={i}>
              <Table.Td  className='align-middle'>{user.name}</Table.Td>
              <Table.Td className='align-middle'>{
                user.teamId ? (
                  user.teamId + ' - ' + user.teamId.name
                ) : (null)
              }</Table.Td>
              <Table.Td className='align-middle'>{user.admin ? 'Si' : 'No'}</Table.Td>
            </Table.TRow>
          )
        )}
      </tbody>
    </Table>
  )
}

export default function LinkUserTeam() {
  let data = useLoaderData()

  let teamOptions = []
  for (let team of data.teams) {
    teamOptions.push(<option key={team.id} value={team.id} >{team.name}</option>)
  }
  let userOptions = []
  for (let user of data.users) {
    userOptions.push(<option key={user.id} value={user.id} >{user.name}</option>)
  }

  return (
    <>
      <div id="teams">
        <Form method="post" id="link-user-team-form">
          <label>Users</label>
          <select name="id" >
            <option key="default" value="Select a User"> -- Select a User -- </option>
            {userOptions}
          </select>
          <br/><br/>
          <label>Teams</label>
          <select name="teamId">
            <option key="default" value="Select a Team"> -- Select a Team -- </option>
            {teamOptions}
          </select>
          <br/>
          <br/>
          <button type="submit">Save</button>
        </Form>
      </div>
    </>
  )
}
