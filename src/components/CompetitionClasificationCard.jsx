
import React from 'react'
import { Link } from 'react-router-dom'
import Table from '@/components/UI/Table/Table'

const CompetitionClasificationCard = ({ competitionClasification }) => {
  return (
    <>

    <div className="max-w-sm mx-2 my-2 w-full">
      <div className="w-full border-solid border-2 border-orange-500 rounded mt-8 bg-white">
        <h5 className="text-center m-4 text-xl font-bold text-black-900">{competitionClasification[0].competition.name}</h5>
      </div>
      <div className="table-responsive border-solid border-2 border-orange-500 rounded overflow-scroll bg-white">
        <Table>
          <Table.THead>
            <Table.TRow>
              <Table.THCol>#</Table.THCol>
              <Table.THCol>Equipo</Table.THCol>
              <Table.THCol>V</Table.THCol>
              <Table.THCol>D</Table.THCol>
              <Table.THCol>PF</Table.THCol>
              <Table.THCol>PC</Table.THCol>
              <Table.THCol>P</Table.THCol>
            </Table.TRow>
          </Table.THead>
          <tbody>
              {competitionClasification.length === 0 ? (
                <p className=''>
                 No hay equipos apuntados a esta competición
                </p>
              ) : (
                competitionClasification.map((competitionTeam,i) =>
                  <Table.TRow key={i}>
                    <Table.Td>{i +1}</Table.Td>
                    <Table.Td>
                      <Link to={`/team-games/${competitionTeam.competition.id}/${competitionTeam.team.id}`}>
                      <div className="flex justify-center items-center">
                        <img
                          src={`${competitionTeam.team.photo}`}
                          className='block'
                          loading="lazy"
                          style={{ width: '3rem', height: '3rem' }}
                          alt='Logo'
                        />
                        </div>
                        <p className='inline-block text-xs'>{competitionTeam.team.name}</p>
                      </Link>
                    </Table.Td>
                    <Table.TdGrey className='align-middle'>{Number(competitionTeam.victories)}</Table.TdGrey>
                    <Table.Td className='align-middle'>{Number(competitionTeam.defeats)}</Table.Td>
                    <Table.TdGrey className='align-middle'>{Number(competitionTeam.pointsScored)}</Table.TdGrey>
                    <Table.Td className='align-middle'>{Number(competitionTeam.pointsReceived)}</Table.Td>
                    <Table.TdGrey className='align-middle'>{Number(competitionTeam.competitionPoints)}</Table.TdGrey>
                  </Table.TRow>
                )
              )}
          </tbody>
        </Table>
      </div>
    </div>
    </>
  )
}

export default CompetitionClasificationCard
