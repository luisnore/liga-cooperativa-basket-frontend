import React from 'react'
import { cn } from '../../lib/utils'

const CloseIcon = ({ className }) => {
  return (
    <svg
      className={cn('h-8 w-8 stroke-emerald-500', className)}
      fill='none'
      viewBox='0 0 24 24'
      strokeWidth='1.5'
      stroke='currentColor'
      aria-hidden='true'
    >
      <path
        strokeLinecap='round'
        strokeLinejoin='round'
        d='M6 18L18 6M6 6l12 12'
      />
    </svg>
  )
}

export default CloseIcon
