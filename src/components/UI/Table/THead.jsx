import React from 'react'

const THead = ({ children }) => {
  return (
    <thead className='text-xs text-center text-gray-700 uppercase border-solid border-b-2 border-orange-500'>
      {children}
    </thead>
  )
}

export default THead
