import React from 'react'

const THRow = ({ children }) => {
  return (
    <tr
      scope='row'
      className='px-1 py-1 text-gray-900 whitespace-nowrap dark:text-white'
    >
      {children}
    </tr>
  )
}

export default THRow
