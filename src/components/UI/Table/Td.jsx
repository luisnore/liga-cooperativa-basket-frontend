import React from 'react'

const Td = ({ children , styles }) => {
  if (styles != undefined){
    return <td className={'px-1 py-1 text-center ' + styles}>{children}</td>
  } else {
    return <td className={'px-1 py-1 text-center '}>{children}</td>
  }
}

export default Td
