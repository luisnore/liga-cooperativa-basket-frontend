import React from 'react'

const TdGrey = ({ children , styles }) => {
  if (styles != undefined){
    return <td className={'px-1 py-1 text-center font-semibold text-slate-600 bg-slate-200' + styles}>{children}</td>
  } else {
    return <td className={'px-1 py-1 text-center font-semibold text-slate-600 bg-slate-200'}>{children}</td>
  }
}

export default TdGrey
