import React from 'react'

const THCol = ({ children }) => {
  return (
    <th scope='col' className='px-1 py-1'>
      {children}
    </th>
  )
}

export default THCol
