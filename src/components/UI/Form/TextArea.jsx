import React from 'react'
import { cn } from '../../../lib/utils'

const TextArea = ({ type, id, text, ...props }) => {
  return (
    <div className={'sm:col-span-3'}>
      <div className='mt-2'>
        <textarea
          type={type}
          name={id}
          id={id}
          placeholder={text}
          {...props}
          autoComplete='given-name'
          rows="3"
          className={cn('block w-full rounded-md border-0  py-1.5 pl-3 text-gray-900 shadow-sm ring-1 ring-inset ring-orange-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset hover:ring-orange-600 focus:ring-orange-600 sm:text-sm sm:leading-6', props.disabled && 'opacity-50 cursor-not-allowed')}
        />
      </div>
    </div>
  )
}

export default TextArea
