import React from 'react'
import Input from './Input'
import TextArea from './TextArea'
import Button from './Button'
import Checkbox from './Checkbox'
import CheckboxWithDefault from './CheckboxWithDefault'
import Select from './Select'
import { Form as FormRouter } from 'react-router-dom'

const Form = ({ children, method = 'post' }) => {
  return (
    <FormRouter
      method={method}
      className='mx-auto max-w-lg bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'
    >
      {children}
    </FormRouter>
  )
}

Form.Input = Input
Form.TextArea = TextArea
Form.Button = Button
Form.Checkbox = Checkbox
Form.CheckboxWithDefault = CheckboxWithDefault
Form.Select = Select

export default Form
