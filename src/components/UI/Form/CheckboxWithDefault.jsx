import React from 'react'

const CheckboxWithDefault = ({ id, text, value, setValue }) => {
  return (
    <div className='relative flex gap-x-3 mt-2 ml-1'>
      <div className='flex h-6 items-center'>
        <input
          id={id}
          name={id}
          defaultChecked={value}
          onClick={() => {setValue(!value)}}
          type='checkbox'
          className='h-4 w-4 rounded border-gray-300 text-orange-600 focus:ring-orange-600'
        />
      </div>
      <div className='text-sm leading-6'>
        <label htmlFor={id} className='font-medium text-gray-900'>
          {text}
        </label>
      </div>
    </div>
  )
}

export default CheckboxWithDefault
