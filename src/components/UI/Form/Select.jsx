import React from 'react'

const Select = ({ id, ...props }) => {
  return (
    <div className="sm:col-span-3">
      <div className="mt-2 inline-block">
        <select
          name={id}
          id={id}
          {...props}
          className="block w-full rounded-md border-0  py-1.5 pl-3 text-gray-900 shadow-sm ring-1 ring-inset ring-orange-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset hover:ring-orange-600 focus:ring-orange-600 sm:text-sm sm:leading-6"
        />
      </div>
    </div>
  )
}

export default Select
