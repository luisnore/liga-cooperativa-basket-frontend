import React, { useEffect, useState } from 'react'
import { Await, json, redirect, useLoaderData, defer } from 'react-router-dom'
import Form from '@/components/UI/Form/Form'
import Table from '@/components/UI/Table/Table'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'
import Spinner from '@/components/UI/Spinner'
import LinkButton from '@/components/UI/LinkButton'

import { getCompetitions } from '../services/competitions'
import { getTeams } from '../services/teams'

export async function newCompetitionAction({ request }) {
  let formData = await request.formData()
  const competition = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.competition}`,
    'POST',
    competition
  )
  return redirect('/admin/newcompetition')
}

export async function newCompetitionTypeAction({ request }) {
  let formData = await request.formData()
  const competitionType = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'POST',
    competitionType
  )
  return redirect('/admin')
}

export async function AddCompetitionTeamAction({ request }) {
  let formData = await request.formData()
  const addTeam = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.competition_team}`,
    'POST',
    addTeam
  )
  return redirect('/admin')
}

export async function loaderNewCompetition(){
  let competitionGroups = await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'GET'
  )
  let competitions = await fetchData(
    `${apiUrl}${serverEndpoints.competition}?expanded=true`,
    'GET'
  )

  return { competitionGroups, competitions }
}

export function AddTeamCompetition() {
  const [teams, setTeams] = useState()
  const [competitions, setCompetitions] = useState()

  useEffect(() => {
    getTeams('')
      .then(teams => {
        const html = []
        for (let team of teams.data) {
          html.push(<option key={team.id} value={team.id} >{team.name}</option>)
        }
        setTeams(html)
        getCompetitions('')
          .then(competitions => {
            const html = []
            for (let comp of competitions.data) {
              html.push(<option key={comp.id} value={comp.id} >{comp.name}</option>)
            }
            setCompetitions(html)
          })

      })
      .catch((error) => {
        throw json(
          { message: error},
          { status: 400 }
        )
      })
  }, [])

  return (
    <div id="contact-form">
      <Form method="post" id="add-team-competition-form">
        <label>TEAM</label>
        <select name="teamId">
          <option key="default" value="Select a Team"> -- Select a Team -- </option>
          {teams}
        </select>
        <br/>
        <br/>
        <label>COMPETITON</label>
        <select name="competitionId" >
          <option key="default" value="Select a Competition"> -- Select a Competition -- </option>
          {competitions}
        </select>
        <br/>
        <br/>
        <button type="submit">Save</button>
      </Form>
    </div>
  )
}

export function NewCompetition() {
  const data = useLoaderData()

  const htmlCompGroups = []
  for (let comp of data.competitionGroups) {
    htmlCompGroups.push(<option key={comp.id} value={comp.id} >{comp.name}</option>)
  }

  return (
    <>
      <Form method="post" id="new-user-form">
        <span>Nombre</span>
        <Form.Input
          placeholder="Competition Name"
          aria-label="Name"
          type="text"
          name="name"
        />
        <br/>
        <span>Fase:</span>
        <Form.Select name="competitionGroupId" >
          {htmlCompGroups}
        </Form.Select>
        <br/>
        <p>
          <Form.Button type="submit">Guardar</Form.Button>
          <Form.Button type="button">Cancelar</Form.Button>
        </p>
      </Form>
      <ListCompetitions/>
    </>
  )
}

export function NewCompetitionType() {

  return (
    <div id="contact-form">
      <Form method="post" id="new-user-form">
        <label>
          <span>Competition Type Name</span>
          <input
            placeholder="Competition Type"
            aria-label="Competition Type Name"
            type="text"
            name="name"
          />
        </label>
        <p>
          <button type="submit">Save</button>
          <button type="button">Cancel</button>
        </p>
      </Form>
    </div>
  )
}

export function ListCompetitions(){
  const data = useLoaderData()

  return(
    <Table>
      <Table.THead>
        <Table.TRow>
          <Table.THCol>Nombre</Table.THCol>
          <Table.THCol>Fase</Table.THCol>
        </Table.TRow>
      </Table.THead>
      <tbody>
        {data.competitions.length === 0 ? (
          <p className=''>
                 No hay equipos apuntados a esta competición
          </p>
        ) : (
          data.competitions.map((competition,i) =>
            <Table.TRow key={i}>
              <Table.Td  className='align-middle'>{competition.name}</Table.Td>
              <Table.Td className='align-middle'>{competition.competitionGroupId} - {competition.competitionGroupId.name}</Table.Td>
            </Table.TRow>
          )
        )}
      </tbody>
    </Table>
  )
}

export async function loaderStatistics(){
  let user = JSON.parse(localStorage.getItem('user'))
  const params = {}

  let competitionGroupId = JSON.parse(localStorage.getItem('activeCompetitionGroupId'))

  let competitionGroups = await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'GET',
  )

  let statistics = null

  try{
    statistics = await fetchData(
      `${apiUrl}${serverEndpoints.competition_team}statistics?competitionGroupId=${competitionGroupId}`,
      'GET',
    )
  } catch {
  }

  return defer({
    competitionGroupId : competitionGroupId,
    competitionGroups: competitionGroups,
    statistics: statistics
  })
}

export function Statistics() {
  const data = useLoaderData()

  function changeCompetitionGroupId(e) {
    localStorage.setItem('activeCompetitionGroupId', e.target.value)
    window.location.reload(false)
  }
  function exportToCsv() {
      exportToCsvFile("datos-liga", data.statistics, [
        "Equipo",
        "Victorias",
        "Derrotas",
        "No Jugados",
        "Fair Play",
        "% Fair Play",
        "Partidos Mixtos",
        "% Partidos Mixtos",
      ])
  }

  if (data.statistics != undefined) {
    return(
    <>
      <React.Suspense>
        <Await resolve={data.competitionGroups}>
          {(competitionGroups) =>
            <div className="flex flex-wrap justify-center">
              <Form.Select onChange={changeCompetitionGroupId} value={data.competitionGroupId}>
                {competitionGroups.map((competitionGroup) => (
                  <option key={competitionGroup.id} value={competitionGroup.id}>
                      {competitionGroup.name} / {competitionGroup.endDate.substring(0,4)}
                  </option>
                ))}
              </Form.Select>
            </div>
          }
        </Await>
      </React.Suspense>
      <React.Suspense fallback={<><Spinner /></>} >
        <Await resolve={data.statistics}>
          {(statistics) =>
            <><div className="table-responsive border-solid border-2 border-orange-500 rounded mt-8 overflow-scroll">
              <Table>
                <Table.THead>
                  <Table.TRow>
                    <Table.THCol>Equipo</Table.THCol>
                    <Table.THCol>Victorias</Table.THCol>
                    <Table.THCol>Derrotas</Table.THCol>
                    <Table.THCol>No Jugados</Table.THCol>
                    <Table.THCol>Fair Play</Table.THCol>
                    <Table.THCol>% Fair Play</Table.THCol>
                    <Table.THCol>Partidos Mixtos</Table.THCol>
                    <Table.THCol>% Partidos Mixtos</Table.THCol>
                  </Table.TRow>
                </Table.THead>
                <tbody>
                  {statistics.map((statistic,i) =>
                    <Table.TRow key={i}>
                      <Table.Td><p className='inline-block ml-4'>{statistic.team_id}</p></Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.victories)}</Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.defeats)}</Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.not_played)}</Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.fair_play)}</Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.porcentaje_fp)}</Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.mixed_games)}</Table.Td>
                      <Table.Td className='align-middle'>{Number(statistic.porcentaje_mixto)}</Table.Td>
                    </Table.TRow>
                  )}
                </tbody>
              </Table>
            </div>

            <LinkButton url='/' text='Volver' type='submit' variant='subtle' className="bg-white border-solid border-2 border-orange-500 rounded mt-8">
                  Volver
            </LinkButton>

            <button onClick={() => {exportToCsv()}} text='Exportar' variant='subtle' className="bg-white border-solid border-2 border-orange-500 rounded mt-8">
                  Exportar
            </button>
            </>
          }
        </Await>
      </React.Suspense>
    </> )
  } else {
    return(
      <>
        <React.Suspense>
          <Await resolve={data.competitionGroups}>
            {(competitionGroups) =>
              <div className="flex flex-wrap justify-center">
                <Form.Select onChange={changeCompetitionGroupId} value={data.competitionGroupId}>
                  {competitionGroups.map((competitionGroup) => (
                    <option key={competitionGroup.id} value={competitionGroup.id}>
                        {competitionGroup.name} / {competitionGroup.endDate.substring(0,4)}
                    </option>
                  ))}
                </Form.Select>
              </div>
            }
          </Await>
          <div className="border-solid border-2 border-orange-500 rounded bg-white px-2 py-2 mx-2 my-2">
            Selecciona las competiciones para filtrar las estadísticas
          </div>
        </React.Suspense>
      </>
    )
  }
}

function exportToCsvFile(filename, rows, headers){
    if (!rows || !rows.length) {
        return;
    }
    const separator = ",";
    const keys  = Object.keys(rows[0]);
    let columHearders = headers;

    const csvContent =
        "sep=,\n" +
        columHearders.join(separator) +
        '\n' +
        rows.map(row => {
            return keys.map(k => {
                let cell = row[k] === null || row[k] === undefined ? '' : row[k];

                cell = cell instanceof Date
                    ? cell.toLocaleString()
                    : cell.toString().replace(/"/g, '""');

                if (cell.search(/("|,|\n)/g) >= 0) {
                    cell = `"${cell}"`;
                }
                return cell;
            }).join(separator);
        }).join('\n');

    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // In case of IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        const link = document.createElement('a');
        if (link.download !== undefined) {
            // Browsers that support HTML5 download attribute
            const url = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}
