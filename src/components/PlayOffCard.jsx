import React from 'react'
import { Link } from 'react-router-dom'
import Table from '@/components/UI/Table/Table'

const PlayOffCard = ({ competition }) => {
  return (
    <>

    <div className="mx-2 my-2 w-full">
      <div className="w-full border-solid border-2 border-orange-500 rounded mt-8 bg-white">
        <h5 className="text-center m-4 text-xl font-bold text-black-900">{competition.name}</h5>
      </div>

      <div className="table-responsive border-solid border-2 border-orange-500 rounded overflow-scroll bg-white">
        <Table>
          <tbody>
              {competition.rounds.length === 0 ? (
                <p className=''>
                 Aún no hay partidos jugados
                </p>
              ) : (
                competition.rounds.map((round,i) =>
                <>
                  <Table.TRow key={(i*2)}>
                    <Table.Td5 styles="text-l font-bold">Ronda {round.round + 1}</Table.Td5>
                  </Table.TRow>
                  <Table.TRow key={(i*2+1)}>
                  {round.pairs.map((pair) => {
                    return (
                      <Table.Td styles="justify-center items-center">
                      <div className="border-solid border-2 border-orange-500 rounded m-auto pt-2 h-24 w-52">
                        <div className="inline-block align-top">
                          <Link to={`/team-games/${competition.id}/${pair.localTeam.id}`}>
                            <div className="flex">
                              <img
                              src={`${pair.localTeam.photo}`}
                              className='block'
                              loading="lazy"
                              style={{ width: '3rem', height: '3rem' }}
                              alt='Logo'
                              />
                            </div>
                            <p className='inline-block text-xs font-bold'
                            style={{ width: '3rem'}}>{pair.localTeam.name}</p>
                          </Link>
                        </div>
                        <div className="inline-block mx-2 align-top">
                          {pair.results.map((result) => {
                          return (
                            <div>
                            {
                              result[0] >= result[1] ?
                                (<><span className='text-emerald-500 font-semibold'>{result[0]}</span>-<span className='text-500'>{result[1]}</span></>)
                                :
                                (<><span className='text-500'>{result[0]}</span>-<span className='text-emerald-500 font-semibold'>{result[1]}</span></>)
                            }
                            </div>
                          );
                        })}
                        </div>
                        <div className="inline-block align-top">
                          <Link to={`/team-games/${competition.id}/${pair.visitantTeam.id}`}>
                            <div className="flex">
                              <img
                              src={`${pair.visitantTeam.photo}`}
                              className='block'
                              loading="lazy"
                              style={{ width: '3rem', height: '3rem' }}
                              alt='Logo'
                              />
                            </div>
                            <p className='inline-block text-xs font-bold'
                            style={{ width: '3rem'}}>{pair.visitantTeam.name}</p>
                          </Link>
                        </div>
                        </div>
                      </Table.Td>
                    );
                  })}
                  </Table.TRow>
                </>
                )
              )}
          </tbody>
        </Table>
      </div>
    </div>
    </>
  )
}

export default PlayOffCard
