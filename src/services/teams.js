import axios from 'axios'
import { json } from 'react-router-dom'
import { apiUrl, serverEndpoints } from '../config'


export async function getTeams(params) {
  let response = axios
    .get(`${apiUrl}${serverEndpoints.team}${params}`)
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function getCompetitionTeams(params) {
  let response = axios
    .get(`${apiUrl}${serverEndpoints.competition_teams}${params}?expanded=true`)
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function getPendingTeams() {
  let user = JSON.parse(localStorage.getItem('user'))
  if (!user.competitionIds.length === 0) {
    throw json(
      { message: 'You dont have a team assigned yet. Please ask it' },
      { status: 400 }
    )
  }

  const params = {
    competitionId: user.activeCompetitionId,
    teamId: user.teamId
  }

  let response = await axios
    .get(`${apiUrl}team/${params.teamId}/pending-games`,
      { params: params },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function checkPendingFairplay(teamId) {
  let user = JSON.parse(localStorage.getItem('user'))

  let response = await axios
    .get(`${apiUrl}team/${teamId}/check-pending-fairplay`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}
