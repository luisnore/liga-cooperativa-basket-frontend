import axios from 'axios'
import { json } from 'react-router-dom'
import { apiUrl, serverEndpoints } from '../config'


export async function createGame(params) {
  let user = localStorage.getItem('user')
  user = JSON.parse(user)
  let token = user.token
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  params.localId = user.teamId

  let response = await axios
    .post(`${apiUrl}${serverEndpoints.game_create}`,
      params,
      {
        headers:
        {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function getGameInfo(params) {
  let response = await axios
    .get(`${apiUrl}${serverEndpoints.game}` + params,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function updateScoreGame(params) {
  let user = localStorage.getItem('user')
  user = JSON.parse(user)
  let token = user.token
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  let response = await axios
    .put(`${apiUrl}${serverEndpoints.game_result}${params.id}`,
      params,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function updateFeedback(params) {
  let user = localStorage.getItem('user')
  user = JSON.parse(user)
  let token = user.token
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  let response = await axios
    .put(`${apiUrl}${serverEndpoints.game_feeback}${params.id}`,
      params,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  return response
}

export async function getGamesByTeam() {
  let user = localStorage.getItem('user')
  user = JSON.parse(user)
  let params = '?expanded=true&localId=' + user.teamId
  let local = await axios
    .get(`${apiUrl}${serverEndpoints.game}${params}`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  params = '?expanded=true&visitantId=' + user.teamId
  let visitant = await axios
    .get(`${apiUrl}${serverEndpoints.game}${params}`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      throw json(
        { message: error },
        { status: 400 }
      )
    })
  const response = [...local.data, ...visitant.data]
  return response
}
