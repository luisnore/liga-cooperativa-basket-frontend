import React, { useEffect } from 'react'
import Form from '@/components/UI/Form/Form'
import { useNavigate, useNavigation } from 'react-router-dom'
import { apiUrl, serverEndpoints } from '../config'
import { Md5 } from 'ts-md5'
import { redirect } from 'react-router-dom'
import { useUserContext } from '@/context/UserContext'

const Login = ({ error = false }) => {
  const navigation = useNavigation()
  const navigate = useNavigate()
  const { user } = useUserContext()
  useEffect(() => {
    if (user) {
      navigate('/')
    }
  }, [])

  return (
    <section className='relative top-24'>
      <Form replace>
        <h1 className='font-bold mb-2'>Login</h1>
        <Form.Input id='name' type='text' text='Nombre' />
        <Form.Input id='password' type='password' text='Contraseña' />
        <Form.Button
          type='submit'
          className='mt-4'
          isLoading={navigation.state === 'submitting'}
        >
          {navigation.state === 'submitting' ? 'Entrando...' : 'Login'}
        </Form.Button>
        {error && (
          <p className='text-red-600 mt-2'>Usuario o contraseña incorrectas</p>
        )}
      </Form>
    </section>
  )
}
/* eslint-disable indent */
export const loginAction =
  userContext =>
  async ({ request }) => {
    const { setUser } = userContext
    let formData = await request.formData()
    const loginData = Object.fromEntries(formData)
    loginData.password = Md5.hashStr(loginData.password)
    const url = `${apiUrl}${serverEndpoints.login}`
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic   ' + btoa(loginData.name + ':' + loginData.password),
      }
    }
    const response = await fetch(url, options)
    const data = await response.json()
    if (data) {
      if (data.competitions != undefined) {
        data.activeCompetitionId = data.competitions[0].id
      }
      localStorage.setItem('user', JSON.stringify(data))
      setUser(data)
      return redirect('/profile')
    }
    return data
  }

export default Login
