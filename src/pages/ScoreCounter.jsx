import React, { useState, useEffect, useCallback } from 'react'
import Input from '../components/UI/Form/Input'
import Button from '../components/UI/Form/Button'
import { debounce } from '../lib/utils'
import Modal from '../components/Modal'
import HornIcon from '../components/Icons/HornIcon'
import hornSound from '../asset/horn.mp3'

const ScoreCounter = () => {
  const [homeScore, setHomeScore] = useState(0)
  const [awayScore, setAwayScore] = useState(0)
  const [homeFaults, setHomeFaults] = useState(0)
  const [awayFaults, setAwayFaults] = useState(0)
  const [quarters, setQuarters] = useState(4)
  const [timePerQuarter, setTimePerQuarter] = useState(12)
  const [currentQuarter, setCurrentQuarter] = useState(1)
  const [timeLeft, setTimeLeft] = useState(timePerQuarter * 60)
  const [isRunning, setIsRunning] = useState(false)
  const [quarterFinished, setQuarterFinished] = useState(false)
  const [gameStarted, setGameStarted] = useState(false)

  const [activeModal, setActiveModal] = useState(false)

  const saveGameState = debounce(state => {
    localStorage.setItem('basketballGameState', JSON.stringify(state))
  }, 500)

  useEffect(() => {
    const savedState = localStorage.getItem('basketballGameState')
    if (savedState) {
      const {
        homeScore,
        awayScore,
        homeFaults,
        awayFaults,
        quarters,
        timePerQuarter,
        currentQuarter,
        timeLeft,
        isRunning,
        quarterFinished,
        gameStarted,
      } = JSON.parse(savedState)
      setHomeScore(homeScore)
      setAwayScore(awayScore)
      setHomeFaults(homeFaults)
      setAwayFaults(awayFaults)
      setQuarters(quarters)
      setTimePerQuarter(timePerQuarter)
      setCurrentQuarter(currentQuarter)
      setTimeLeft(timeLeft)
      setIsRunning(isRunning)
      setQuarterFinished(quarterFinished)
      setGameStarted(gameStarted)
    }
  }, [])

  useEffect(() => {
    saveGameState({
      homeScore,
      awayScore,
      homeFaults,
      awayFaults,
      quarters,
      timePerQuarter,
      currentQuarter,
      timeLeft,
      isRunning,
      quarterFinished,
      gameStarted,
    })
  }, [
    homeScore,
    awayScore,
    homeFaults,
    awayFaults,
    quarters,
    timePerQuarter,
    currentQuarter,
    timeLeft,
    isRunning,
    quarterFinished,
    gameStarted,
  ])

  useEffect(() => {
    let interval
    if (isRunning && timeLeft > 0) {
      interval = setInterval(() => {
        setTimeLeft(prevTime => prevTime - 1)
      }, 1000)
    } else if (timeLeft === 0) {
      setIsRunning(false)
      setQuarterFinished(true)
      playHorn()
    }
    return () => clearInterval(interval)
  }, [isRunning, timeLeft])

  const updateScore = (team, points) => {
    if (team === 'home') {
      setHomeScore(prevScore => Math.max(0, prevScore + points))
    } else {
      setAwayScore(prevScore => Math.max(0, prevScore + points))
    }
  }

  const updateFaults = (team, faults) => {
    if (team === 'home') {
      setHomeFaults(prevFaults => Math.max(0, prevFaults + faults))
    } else {
      setAwayFaults(prevFaults => Math.max(0, prevFaults + faults))
    }
  }

  const resetGame = () => {
    setHomeScore(0)
    setAwayScore(0)
    setHomeFaults(0)
    setAwayFaults(0)
    setCurrentQuarter(1)
    setTimeLeft(timePerQuarter * 60)
    setIsRunning(false)
    setQuarterFinished(false)
    setGameStarted(false)
  }

  const startStopTimer = () => {
    if (!gameStarted) {
      setGameStarted(true)
    }
    if (!quarterFinished) {
      setIsRunning(!isRunning)
    }
  }

  const resetTimer = () => {
    setTimeLeft(timePerQuarter * 60)
    setIsRunning(false)
    setQuarterFinished(false)
  }

  const nextQuarter = useCallback(() => {
    if (currentQuarter + 1 <= quarters) {
      setCurrentQuarter(prev => prev + 1)
    }
    setTimeLeft(timePerQuarter * 60)
    setHomeFaults(0)
    setAwayFaults(0)
    setIsRunning(false)
    setQuarterFinished(false)
  }, [currentQuarter, quarters, timePerQuarter, quarterFinished])

  const formatTime = seconds => {
    const mins = Math.floor(seconds / 60)
    const secs = seconds % 60
    return `${mins.toString().padStart(2, '0')}:${secs
      .toString()
      .padStart(2, '0')}`
  }

  const openModal = modalType => {
    setActiveModal(modalType)
  }

  const closeModal = () => {
    setActiveModal(null)
  }

  const renderModalContent = activeModal => {
    switch (activeModal) {
      case 'nextQuarter':
        return (
          <div>
            <p className='font-semibold mb-4'>
              ¿Estás seguro de que quieres avanzar al próximo cuarto?
            </p>
            <Button
              className='w-full mb-2'
              onClick={() => {
                nextQuarter()
                closeModal()
              }}
            >
              Próximo cuarto
            </Button>
          </div>
        )
      case 'resetGame':
        return (
          <div>
            <p className='font-semibold mb-4'>
              ¿Estás seguro de que quieres resetear el partido?
            </p>
            <Button
              className='w-full mb-2'
              onClick={() => {
                resetGame()
                closeModal()
              }}
            >
              Resetear
            </Button>
          </div>
        )
      default:
        return null
    }
  }

  const playHorn = () => {
    return new Audio(hornSound).play()
  }

  return (
    <>
      <div className='p-4 max-w-md mx-auto bg-white text-center'>
        <h1 className='text-2xl font-bold mb-4'>Marcador</h1>

        <div className='text-center mb-4'>
          <div className='grid grid-cols-2 gap-2'>
            <Button
              variant='subtle'
              onClick={startStopTimer}
              disabled={quarterFinished}
            >
              {isRunning ? 'Pausar' : 'Empezar'}
            </Button>

            <Button onClick={() => openModal('nextQuarter')}>
              Próximo cuarto
            </Button>
          </div>
        </div>

        <div className='mb-4'></div>

        <div className='text-center mb-4'>
          <p className='text-3xl font-bold mb-2'>{formatTime(timeLeft)}</p>
          <p className='text-lg'>
            Tiempo: {currentQuarter} / {quarters}
          </p>
        </div>

        <div className='flex justify-between mb-4'>
          <div className='text-center mr-2'>
            <div className='text-xl font-semibold'>
              <Input
                id='local'
                type='string'
                placeholder='Local'
                disabled={gameStarted}
                className='w-full'
              />
            </div>
            <p className='text-3xl font-bold'>{homeScore}</p>
            <div className='grid grid-cols-2 gap-2 mt-2'>
              <Button onClick={() => updateScore('home', -1)}>-1</Button>
              <Button variant='subtle' onClick={() => updateScore('home', 1)}>
                +1
              </Button>
              <Button variant='subtle' onClick={() => updateScore('home', 2)}>
                +2
              </Button>
              <Button variant='subtle' onClick={() => updateScore('home', 3)}>
                +3
              </Button>
            </div>
          </div>

          <div className='text-center'>
            <div className='text-xl font-semibold'>
              <Input
                id='visitant'
                type='string'
                placeholder='Visitante'
                disabled={gameStarted}
                className='w-full'
              />
            </div>
            <p className='text-3xl font-bold'>{awayScore}</p>
            <div className='grid grid-cols-2 gap-2 mt-2'>
              <Button onClick={() => updateScore('away', -1)}>-1</Button>
              <Button variant='subtle' onClick={() => updateScore('away', 1)}>
                +1
              </Button>
              <Button variant='subtle' onClick={() => updateScore('away', 2)}>
                +2
              </Button>
              <Button variant='subtle' onClick={() => updateScore('away', 3)}>
                +3
              </Button>
            </div>
          </div>
        </div>

        <div className='flex justify-between mb-4'>
          <div className='text-center mr-2'>
            <p className='text-xl semi-font-bold'>Faltas: {homeFaults}</p>
            <div className='grid grid-cols-2 gap-2 mb-2 h-8'>
              <Button onClick={() => updateFaults('home', -1)}>-1</Button>
              <Button variant='subtle' onClick={() => updateFaults('home', 1)}>
                +1
              </Button>
            </div>
          </div>

          <div className='text-center'>
            <p className='text-xl font-semi-bold'>Faltas: {awayFaults}</p>
            <div className='grid grid-cols-2 gap-2 mb-2 h-8'>
              <Button onClick={() => updateFaults('away', -1)}>-1</Button>
              <Button variant='subtle' onClick={() => updateFaults('away', 1)}>
                +1
              </Button>
            </div>
          </div>
        </div>

        <div className='mb-4'>
          <div className='grid grid-cols-4 gap-2'>
            <div className="relative">
              <label htmlFor="quarters" className="absolute bottom-0 block text-gray-500 font-bold md:text-right mb-1 md:mb-0">
                # Cuartos:
              </label>
            </div>
            <div className="columns-[2rem]">
              <Input
                id='quarters'
                type='number'
                value={quarters}
                onChange={e => {
                  const value = parseInt(e.target.value)
                  if (value > 0 && value <= 4) {
                    setQuarters(value)
                  }
                }}
                min='1'
                max='4'
                disabled={gameStarted}
                className='w-full'
              />
            </div>
            <div className="relative">
              <label htmlFor="timePerQuarter" className="absolute bottom-0 block text-gray-500 font-bold md:text-right mb-1 md:mb-0">
                Duración:
              </label>
            </div>
            <div className="columns-[3rem]">
              <Input
                id='timePerQuarter'
                type='number'
                value={timePerQuarter}
                onChange={e => {
                  const value = parseInt(e.target.value)
                  if (value > 0) {
                    setTimePerQuarter(value)
                    setTimeLeft(value * 60)
                  }
                }}
                min='1'
                disabled={gameStarted}
                className='w-full'
              />
            </div>
          </div>
        </div>

        <div className='text-center mb-4'>
          <div className='grid grid-cols-2 gap-2'>
            <Button onClick={() => openModal('resetGame')}>
              Resetear partido
            </Button>
            <Button onClick={resetTimer} disabled={isRunning}>
              Reset tiempo
            </Button>
          </div>
          <Button
            variant='subtle'
            className='mt-2 w-full flex items-center justify-center'
            onClick={playHorn}
          >
            PUH PUH PUH!!! <HornIcon />
          </Button>
        </div>
      </div>

      {activeModal && (
        <Modal isOpen={!!activeModal} onClose={closeModal}>
          {renderModalContent(activeModal)}
        </Modal>
      )}
    </>
  )
}

export default ScoreCounter
