import React from 'react'

const Info = () => {
  return (
    <div className=' text-left'>
      <div className='card-body border-solid border-2 border-orange-500 rounded mt-8 bg-white py-12 px-4'>
      <div className='max-w-2xl mx-auto'>
        <h1 className='font-bold mb-4 text-center'>
          LIGA COOPERATIVA BALONCESTO MADRID – TEMPORADA 2024/2025
        </h1>
        <h2 className='font-semibold mb-10 text-center'>Normas de Juego</h2>
        <p className="font-semibold">No hay árbitro/a en los partidos pero sí unas normas de juego:</p>
        <br></br>
        <ol className='list-decimal list-inside mb-4'>
          <li>
            La fecha, hora y lugar de los partidos se decide por consenso entre
            los dos equipos.
          </li>
          <li>
            Si uno de los dos equipos no puede asistir y no avisa con al menos 3
            horas de antelación, el otro equipo puede darle el partido por
            perdido.
          </li>
          <li>
            Tiempo de juego: 4 cuartos de 12 minutos cada uno.
            El tiempo se para únicamente en los tiros libres de todos los cuartos
             y en los 2 últimos minutos del último cuarto.
          </li>
          <li>
            El marcador lo lleva cualquiera de los dos equipos o los dos y máximo
             hay un tiempo muerto para cada equipo en cada cuarto.
          </li>
          <li>
            En caso de lucha prevalece la flecha para decidir qué equipo
            mantiene la posesión.
          </li>
          <li>
            Las faltas o infracciones son pitadas por la persona/equipo que comete la infracción.
             Siguiendo el coarbitraje el otro equipo puede indicarlo y si hay posiciones contrapuestas, debatirlo.
          </li>
          <li>
            A partir de las 5 faltas de equipo, hay tiros libres para el otro equipo.
          </li>
          <li>
            Si el último jugador/a corta un contraataque sin opción de robar el
            balón se considera falta antideportiva y son 2 tiros libres y posesión
            para el otro equipo (igual con la técnica).
          </li>
          <li>
            Si algún equipo propone jugar con un balón de talla 6, se jugará con él.
            Si el otro equipo está muy en desacuerdo, se jugarán dos cuartos con balón talla 6 y dos cuartos con balón talla 7.
          </li>
          <li>
            Se permite jugar de forma constante en 2 equipos de la Liga a cualquier jugador@.
          </li>
          <li>
            Se puede utilizar la figura de mercenari@ cuando un equipo tiene poca gente
             para un partido pero pidiendo la aprobación del otro equipo y siendo máximo 8 jugador@s en ese partido.
          </li>
          <li>
            En la app se registra tanto si el otro equipo ha jugado un partido
             mixto o no como la puntuación de fairplay que se le da al otro equipo:
             <ul>
                <li className='mb-2 mt-1'>+1 a los equipos que son honestos, respetan la norma de "pita quien comete la infracción"
                 e incluye en su diversión al otro equipo. A esto le añadimos también la facilidad
                  y el trato a la hora de concretar día, hora y cancha del partido. </li>
                <li className='mb-2'>0 a los equipos que cumplen lo anterior aunque en algún ámbito han cometido
                 uno o pocos errores. Ha habido algún gesto que no os ha gustado del todo pero sin más, partido correcto.</li>
                <li className='mb-2'>-1 a los equipos que han incumplido los aspectos mencionados de forma recurrente a lo largo del partido.
                 Lleváis deseando que se acabe el partido durante un rato y esperáis no volver a jugar con ese equipo en esta temporada.</li>
            </ul>
          </li>
        </ol>
        <h3 className='font-semibold mb-6 mt-6 text-center'>Normas de participación:</h3>
        <ol className='list-decimal list-inside mb-4'>
          <li><b>Jugar</b> todos los partidos, o todos menos uno, en cada fase. En la 1ª fase, que es más corta, es obligatorio jugar todos los partidos.</li>
          <li><b>Hacer</b>, al menos, una tarea en la temporada.</li>
          <li><b>Asistir</b> a las 3 asambleas generales.</li>
        </ol>
        <p className ='mb-4'>Si no se cumplen estas normas:</p>
        <div className='mb-4'>
          <h4 className='font-semibold mb-2'>Apercibimiento:</h4>
          <p>
            Actitudes poco deportivas o puntuación muy baja en la clasificación
            del fairplay.
          </p>
        </div>
        <h3 className='font-semibold mb-2'>Expulsión:</h3>
        <ol className='list-decimal list-inside mb-4'>
          <li>No cumplir con las normas de participación</li>
          <li>Intentos de agresión o agresiones</li>
          <li>Acumular más de un apercibimiento</li>
          <li>No respetar los valores de la liga</li>
        </ol>
        <p className ='mb-4 font-semibold'>
          Todas las normas son susceptibles de eliminarse, modificarse o
          añadirse siempre que cualquier jugador/a lo proponga en una asamblea y
          se apruebe.
        </p>
        <p className ='font-semibold'>Ante cualquier duda, ¡hablemos y consensuemos!</p>
        </div>
      </div>
    </div>
  )
}

export default Info
