export { default as Courts } from './Court/Courts'
export { default as NewCourt } from './Court/NewCourt'
export { default as EditCourt } from './Court/EditCourt'

export { default as Team } from './Teams/Team'

export { default as Info } from './Info'
export { default as Login } from './Login'
export { default as Register } from './Register'
export { default as Administration } from './Administration'

export { default as ScheduleGame } from './Games/ScheduleGame'
export { default as UpdateScoreGame } from './Games/UpdateScoreGame'
export { default as IncompleteGamesList } from './Games/IncompleteGamesList'

export { default as TeamGames } from './Games/TeamGames'
export { default as CompetitionClasification } from './Competitions/CompetitionClasification'
export { default as LeaguesClasification } from './Leagues/LeaguesClasification'
export { default as LeaguesClasificationHistory } from './Leagues/LeaguesClasificationHistory'
export { default as GameStored } from './Games/GameStored'
export { default as GamesCalendar } from './Games/GamesCalendar'

export { default as Profile } from './Users/Profile'
