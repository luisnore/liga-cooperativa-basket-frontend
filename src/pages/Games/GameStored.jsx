import { Link, redirect, useLoaderData } from 'react-router-dom'
import React from 'react'
import { getTeams } from '@/services/teams'
import { fetchData } from '@/services/services'
import { apiUrl } from '@/config'
import { serverEndpoints } from '@/config'
import {
  getGameInfo,
} from '@/services/games'

export async function loaderGameStored({ params }) {
  let game = await getGameInfo(params.gameId)
  let localTeam = await getTeams(game.data.localId)
  let visitantTeam = await getTeams(game.data.visitantId)
  let court = await fetchData(
    `${apiUrl}${serverEndpoints.court}${game.data.courtId}`,
    'GET',
  )
  return { game, localTeam, visitantTeam, court }
}

export default function GameStoredList() {
    const data = useLoaderData()
    let game = data.game.data
    let localTeam = data.localTeam.data
    let visitantTeam = data.visitantTeam.data
    let court = data.court

    return (
      <>
        <div className="flex flex-wrap justify-center">
          <div className="max-w-sm mx-2 my-2">
            <div className='flex h-full flex-col border-solid border-2 border-orange-500 rounded bg-white'>
              <div className='py-2 text-center bg-orange-100'>
                <h5 className='font-semibold'>¡Partido Guardado!</h5>
              </div>
              <div className='px-4 py-3 text-center'>
                <div className='mt-2'>
                  <strong className='text-muted align-left'>
                    {game.date.split('.')[0].split('T')[0].split('-')[2]}/
                    {game.date.split('.')[0].split('T')[0].split('-')[1]}/
                    {game.date.split('.')[0].split('T')[0].split('-')[0]}
                  </strong>
                  <strong className='text-muted align-right'>
                    {' '}
                    {game.date.split('.')[0].split('T')[1]}
                  </strong>
                </div>
                <div className='grid grid-cols-3 items-center mt-2'>
                <div className='mt-2'>
                  <strong>
                    <img
                      src={`${localTeam.photo}`}
                      className='mx-auto'
                      style={{ width: '3rem', height: '3rem' }}
                      alt='Logo'
                    />
                    {localTeam.name}</strong>
                </div>
                <div className='mt-2'>
                  <span className='font-bold'>vs</span>
                </div>
                <div className='mt-2'>
                  <img
                    src={`${visitantTeam.photo}`}
                    className='img-thumbnail mx-auto'
                    style={{ width: '3rem', height: '3rem' }}
                    alt='Logo'
                  />
                  <strong>{visitantTeam.name}</strong>
                </div>
                </div>
                <div className='mt-4'>
                  <strong className='text-muted'>
                    {court.name}
                  </strong>
                  <div className='text-muted mt-2'>
                    {court.direction}
                  </div>
                </div>
              </div>
              <div className='pt-2 pb-4 px-2 text-center'>
                <Link
                  to="/"
                >
                  <button type='button' className='btn btn-danger'>Volver</button>
                  <br />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </>
    )
}
