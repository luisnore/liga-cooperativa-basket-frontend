import React, { useEffect, useState } from 'react';
import { redirect, useActionData } from 'react-router-dom';
import Form from '@/components/UI/Form/Form';
import { Md5 } from 'ts-md5';
import { apiUrl, serverEndpoints } from '@/config';

// Acción para actualizar la contraseña
export async function actionUpdateProfile({ request }) {
  const user = JSON.parse(localStorage.getItem('user'));
  const formData = await request.formData();
  const data = Object.fromEntries(formData);

  if (data.newPassword !== data.repeatPassword) {
    return { error: 'Las contraseñas no coinciden' };
  }

  data.oldPassword = Md5.hashStr(data.oldPassword);
  data.newPassword = Md5.hashStr(data.newPassword);

  await fetch(
    `${apiUrl}${serverEndpoints.user}${user.userId}/update-password`,
    {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json',
       Authorization: 'Bearer ' + user?.token,
       },
      body: JSON.stringify(data),
    }
  );

  return redirect('/');
}

const Profile = () => {
  const [team, setTeam] = useState(null);
  const [error, setError] = useState(null);
  const [selectedTab, setSelectedTab] = useState('summary'); // Por defecto, "Resumen" está seleccionado
  const errorAction = useActionData();

  useEffect(() => {
    const fetchTeamData = async () => {
      try {
        const user = JSON.parse(localStorage.getItem('user'));

        if (!user || !user.teamId) {
          setError('No se encontró un equipo asociado a este usuario.');
          return;
        }

        // Obtener datos del equipo
        const responseTeam = await fetch(
          `${apiUrl}${serverEndpoints.team}${user.teamId}`,
          {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${user.token}`,
              'Content-Type': 'application/json',
            },
          }
        );

        if (!responseTeam.ok) throw new Error(`Error HTTP: ${responseTeam.status}`);
        const teamData = await responseTeam.json();
        setTeam(teamData);
      } catch (err) {
        console.error('Error al cargar datos:', err.message);
        setError('No se pudieron cargar los datos.');
      }
    };

    fetchTeamData();
  }, []);

  const renderContent = () => {
    // Si la pestaña seleccionada es "summary", mostrar el embed de Instagram
    if (selectedTab === 'summary') {
      return (
        <div className="flex flex-col md:flex-row">
          <div className="instagram-embed flex-1 md:mr-4 mb-4 md:mb-0">
            <iframe
              src="https://www.instagram.com/ligabasketcooperativa/embed"
              width="100%"
              height="480"
              allowtransparency="true"
            ></iframe>
          </div>
        </div>
      );
    }

    // Si la pestaña seleccionada es "changePassword", mostrar el formulario de cambio de contraseña
    if (selectedTab === 'changePassword') {
      return (
        <Form className="bg-white p-4 rounded shadow">
          <div className="grid gap-4">
            <h2 className="text-lg font-bold">Cambiar contraseña</h2>
            <Form.Input id="oldPassword" type="password" text="Contraseña Actual" />
            <Form.Input id="newPassword" type="password" text="Nueva Contraseña" />
            <Form.Input
              id="repeatPassword"
              type="password"
              text="Repetir contraseña"
            />
            {errorAction && <p className="text-red-600">{errorAction.error}</p>}
            <Form.Button type="submit">Cambiar Contraseña</Form.Button>
          </div>
        </Form>
      );
    }

    return null; // Default case, return null if no valid tab selected
  };

  return (
    <div className="p-6">
      <div className="mb-6 bg-white p-4 rounded shadow">
        <h1 className="text-2xl font-bold mb-4">Perfil</h1>
        {error && <p className="text-red-500 mb-4">{error}</p>}
        {team && (
          <div className="flex justify-between items-center">
            <div className="flex items-center">
              <img
                src={team.photo}
                alt={`Logo del equipo ${team.name}`}
                className="w-24 h-24 rounded-full border border-gray-300 shadow"
              />
              <div className="ml-4">
                <p className="text-lg font-bold">{team.name}</p>
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="bg-white p-4 rounded shadow">
        {/* Men� */}
        <div className="flex gap-4 mb-4">
          <button
            onClick={() => setSelectedTab('summary')}
            className={`px-4 py-2 rounded ${
              selectedTab === 'summary'
                ? 'bg-orange-500 text-white'
                : 'bg-gray-100 text-gray-700'
            }`}
          >
            Resumen
          </button>
          <button
            onClick={() => setSelectedTab('changePassword')}
            className={`px-4 py-2 rounded ${
              selectedTab === 'changePassword'
                ? 'bg-orange-500 text-white'
                : 'bg-gray-100 text-gray-700'
            }`}
          >
            Cambiar Contraseña
          </button>
        </div>
        {/* Contenido dinámico basado en la selección del menú */}
        {renderContent()}
      </div>
    </div>
  );
};

export default Profile;