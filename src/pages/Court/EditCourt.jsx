import React from 'react'
import { useState } from "react"
import { useLoaderData } from 'react-router'
import { redirect, useActionData, useNavigation } from 'react-router-dom'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'
import LinkButton from '@/components/UI/LinkButton'
import Form from '@/components/UI/Form/Form'

export async function editCourtAction({ request }) {
  let formData = await request.formData()
  const courtForm = Object.fromEntries(formData)
  {courtForm.lights != undefined ?
    courtForm.lights = true :
    courtForm.lights = false}
  try {
    await fetchData(`${apiUrl}${serverEndpoints.court}${courtForm.id}`, 'PUT', courtForm)
    return redirect('/courts')
  } catch (err) {
    return err.statusText
  }
}

const EditCourt = () => {
  const court = useLoaderData()
  const errorMessage = useActionData()
  const navigation = useNavigation()
	const [lights, setLights] = useState(court.lights);
  return (
    <>
      <div
        className='max-w-lg border-solid border-2 border-orange-500 rounded bg-white p-0'
      >
        <Form>
          {errorMessage && <h4 style={{ color: 'red' }}>{errorMessage}</h4>}
          <h1 className='font-bold mb-2'>Editar cancha</h1>
          <Form.Input type='hidden' id='id' text='Id' defaultValue={court.id} required/>
          <Form.Input type='text' id='name' text='Nombre' defaultValue={court.name} required/>
          <Form.Input type='text' id='direction' text='Dirección' defaultValue={court.direction} required />
          <Form.Input type='text' id='status' text='Estado de la cancha' defaultValue={court.status}/>
          <Form.Input type='text' id='lightTimeout' text='Apagado de luces' defaultValue={court.lightTimeout}/>
          <Form.CheckboxWithDefault type='checkbox' id='lights' text='Luces' value={lights} setValue={setLights}/>
          <div className='flex gap-4 mt-4'>
            <Form.Button
              type='submit'
              isLoading={navigation.state === 'submitting'}
            >
              {navigation.state === 'submitting' ? 'Guardando...' : 'Guardar'}
            </Form.Button>
            <LinkButton
              url='/courts'
              text='Guardar'
              type='submit'
              variant='subtle'
            >
          Volver
            </LinkButton>
          </div>
        </Form>
      </div>
    </>
  )
}

export default EditCourt
