import React from 'react'
import { Await, defer, useLoaderData } from 'react-router-dom'
import { serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'
import Form from '@/components/UI/Form/Form'
import { apiUrl } from '@/config'
import CompetitionClasificationCard from '@/components/CompetitionClasificationCard'
import PlayOffCard from '@/components/PlayOffCard'
import Spinner from '@/components/UI/Spinner'


export async function loaderLeaguesClasificationHistory() {
  let activeCompetitionGroupId = JSON.parse(localStorage.getItem('activeCompetitionGroupId'))

  let activeCompetitionGroups = await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'GET',
  )
  let activeCompetitionGroup;
  for( let i = 0; i < activeCompetitionGroups.length; i++){
    if ( activeCompetitionGroupId != undefined ) {
      if (activeCompetitionGroups[i].id == activeCompetitionGroupId){
        activeCompetitionGroup = activeCompetitionGroups[i]
        break
      }
    } else {
      if(activeCompetitionGroups[i].active){
        localStorage.setItem('activeCompetitionGroupId', activeCompetitionGroups[i].id)
        activeCompetitionGroup = activeCompetitionGroups[i]
        break
    }
    }
  }

  let competitionClasifications;
  let playOffData;
  if (activeCompetitionGroup.crossing){
    playOffData = fetchData(
      `${apiUrl}${serverEndpoints.game}get-crossing-summary/${activeCompetitionGroup.id}`,
      'GET',
    )
  } else {
    competitionClasifications = fetchData(
      `${apiUrl}${serverEndpoints.competition_team}clasifications?competitionGroupId=${activeCompetitionGroup.id}`,
      'GET',
    )
  }

  return defer({
    competitionClasifications : competitionClasifications,
    playOffData: playOffData,
    activeCompetitionGroups : activeCompetitionGroups,
    activeCompetitionGroupId : activeCompetitionGroupId
  })
}

export default function LeaguesClasificationHistory() {
  const data = useLoaderData()

  function changeCompetitionGroup(e) {
    localStorage.setItem('activeCompetitionGroupId', e.target.value)
    window.location.reload(false)
  }

  if (data.competitionClasifications != undefined){
    return(
    <>
      <React.Suspense>
        <Await resolve={data.activeCompetitionGroups}>
          {(activeCompetitionGroups) =>
            <div className="flex flex-wrap justify-center">
              <Form.Select onChange={changeCompetitionGroup} value={data.activeCompetitionGroupId}>
                {activeCompetitionGroups.map((competitionGroup) => {
                  return (
                    <option
                      key={competitionGroup.id}
                      value={competitionGroup.id}
                      className={!competitionGroup.active ? 'text-slate-500': ''}
                    >
                      {competitionGroup.name} / {competitionGroup.endDate.substring(0,4)}
                    </option>
                  )
                })}
              </Form.Select>
            </div>
          }
        </Await>
      </React.Suspense>
      <React.Suspense fallback={<><Spinner /></>} >
        <Await resolve={data.competitionClasifications}>
          {(competitionClasifications) =>
            <div className="flex flex-wrap justify-center">
              {competitionClasifications.length === 0 ? (
                <div className="border-solid border-2 border-orange-500 rounded bg-white px-2 py-2 mx-2 my-2">
             No hay competiciones
                </div>
              ) : (
                competitionClasifications.map(competitionClasification =>
                  <CompetitionClasificationCard
                    key={competitionClasification[0].competition.id}
                    competitionClasification={competitionClasification}
                  />))
              }
            </div>
          }
        </Await>
      </React.Suspense>
    </>
  )
} else {
  return(
    <>
      <React.Suspense>
        <Await resolve={data.activeCompetitionGroups}>
          {(activeCompetitionGroups) =>
            <div className="flex flex-wrap justify-center">
              <Form.Select onChange={changeCompetitionGroup} value={data.activeCompetitionGroupId}>
                {activeCompetitionGroups.map((competitionGroup) => {
                  return (
                    <option
                      key={competitionGroup.id}
                      value={competitionGroup.id}
                      className={!competitionGroup.active ? 'text-slate-500': ''}
                    >
                      {competitionGroup.name}
                    </option>
                  )
                })}
              </Form.Select>
            </div>
          }
        </Await>
      </React.Suspense>
      <React.Suspense fallback={<><Spinner /></>} >
        <Await resolve={data.playOffData}>
          {(playOffData) =>
            <div className="flex flex-wrap justify-center">
              {playOffData.competitions.length === 0 ? (
                <div className="border-solid border-2 border-orange-500 rounded bg-white px-2 py-2 mx-2 my-2">
             No hay competiciones
                </div>
              ) : (
                playOffData.competitions.map(competition =>
                  <PlayOffCard
                    key={competition.name}
                    competition={competition}
                  />))
              }
            </div>
          }
        </Await>
      </React.Suspense>
    </>
  )
}


}
