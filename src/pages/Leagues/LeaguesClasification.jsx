import React from 'react'
import { Await, defer, useLoaderData, useAsyncError } from 'react-router-dom'
import { serverEndpoints, apiUrl } from '@/config'
import { fetchData } from '@/services/services'
import Form from '@/components/UI/Form/Form'
import CompetitionClasificationCard from '@/components/CompetitionClasificationCard'
import PlayOffCard from '@/components/PlayOffCard'
import Spinner from '@/components/UI/Spinner'

export async function loaderLeaguesClasification() {
  try {
    const activeCompetitionGroupId = JSON.parse(localStorage.getItem('activeCompetitionGroupId'))

    const activeCompetitionGroups = await fetchData(
      `${apiUrl}${serverEndpoints.competition_group}?leagueId=2`,
      'GET',
    )

    if (!Array.isArray(activeCompetitionGroups) || activeCompetitionGroups.length === 0) {
      throw new Error('No hay competiciones disponibles ahora mismo')
    }

    const activeCompetitionGroup = activeCompetitionGroups.find(group =>
      activeCompetitionGroupId ? group.id == activeCompetitionGroupId : group.active
    ) || activeCompetitionGroups[0]

    if (!activeCompetitionGroupId) {
      localStorage.setItem('activeCompetitionGroupId', activeCompetitionGroup.id)
    }

    let competitionClasifications, playOffData
    if (activeCompetitionGroup.crossing) {
      playOffData = fetchData(
        `${apiUrl}${serverEndpoints.game}get-crossing-summary/${activeCompetitionGroup.id}`,
        'GET',
      )
    } else {
      competitionClasifications = fetchData(
        `${apiUrl}${serverEndpoints.competition_team}clasifications?competitionGroupId=${activeCompetitionGroup.id}`,
        'GET',
      )
    }

    return defer({
      competitionClasifications,
      playOffData,
      activeCompetitionGroups,
      activeCompetitionGroupId: activeCompetitionGroup.id
    })
  } catch (error) {
    console.error('Error in loaderLeaguesClasification:', error)
    return defer({
      error: error instanceof Error ? error.message : 'An unknown error occurred'
    })
  }
}

function ErrorFallback() {
  const error = useAsyncError()
  return (
    <div className="p-4 border border-red-300 rounded grid mt-40">
      <h2>Algo ha ido mal:</h2>
      <p>{error instanceof Error ? error.message : 'No se han podido cargar los datos'}</p>
    </div>
  )
}

export default function LeaguesClasification() {
  const data = useLoaderData()

  function changeCompetitionGroup(e) {
    localStorage.setItem('activeCompetitionGroupId', e.target.value)
    window.location.reload(false)
  }

  if (data.error) {
    return <div className="p-4 border border-red-300 rounded bg-white mt-40">
      <h2>Ha habido un error:</h2>
      <p>{data.error}</p>
    </div>
  }

  return (
    <>
      <React.Suspense fallback={<Spinner />}>
        <Await resolve={data.activeCompetitionGroups} errorElement={<ErrorFallback />}>
          {(activeCompetitionGroups) => (
            <div className="flex flex-wrap justify-center">
              <Form.Select onChange={changeCompetitionGroup} value={data.activeCompetitionGroupId}>
                {activeCompetitionGroups.map((competitionGroup) => (
                  <option
                    key={competitionGroup.id}
                    value={competitionGroup.id}
                    className={!competitionGroup.active ? 'text-slate-500' : ''}
                  >
                    {competitionGroup.name}
                  </option>
                ))}
              </Form.Select>
            </div>
          )}
        </Await>
      </React.Suspense>
      <React.Suspense fallback={<Spinner />}>
        <Await
          resolve={data.competitionClasifications || data.playOffData}
          errorElement={<ErrorFallback />}
        >
          {(resolvedData) => (
            <div className="flex flex-wrap justify-center">
              {!resolvedData || resolvedData.length === 0 ? (
                <div className="border-solid border-2 border-orange-500 rounded bg-white px-2 py-2 mx-2 my-2">
                  No hay competiciones
                </div>
              ) : data.competitionClasifications ? (
                resolvedData.map(competitionClasification => (
                  <CompetitionClasificationCard
                    key={competitionClasification[0].competition.id}
                    competitionClasification={competitionClasification}
                  />
                ))
              ) : (
                resolvedData.competitions.map(competition => (
                  <PlayOffCard
                    key={competition.name}
                    competition={competition}
                  />
                ))
              )}
            </div>
          )}
        </Await>
      </React.Suspense>
    </>
  )
}
