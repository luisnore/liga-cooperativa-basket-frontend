import React from 'react'
import { Await, defer, redirect, useAsyncValue, useLoaderData } from 'react-router-dom'
import Form from '@/components/UI/Form/Form'
import Table from '@/components/UI/Table/Table'


import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'

export async function newCompetitionGroupAction({ request }) {
  let formData = await request.formData()
  const competitionGroup = Object.fromEntries(formData)
  competitionGroup.active = true
  await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'POST',
    competitionGroup
  )
  return redirect('/admin/newcompetitiongroup')
}

export async function loaderNewCompetitionGroup(){
  let leagues = fetchData(
    `${apiUrl}${serverEndpoints.league}`,
    'GET'
  )

  let groups = fetchData(
    `${apiUrl}${serverEndpoints.competition_group}` + '?expanded=true',
    'GET'
  )
  return defer({
    leagues: leagues,
    groups: groups })
}

export function NewCompetitionGroup() {
  const data = useLoaderData()

  return (
    <>
      <Form >
        <div className='grid gap-4'>
          <h1 className='font-bold'>Crear Fase</h1>
          <br/>
        </div>
        <span>Name</span>
        <Form.Input
          placeholder="Nombre de Fase"
          aria-label="Name"
          type="text"
          name="name"
        />
        <br/>
        <span>Liga/Temporada</span>
        <React.Suspense fallback={<p className="bg-white align-middle">Cargando ligas...</p>}>
          <Await
            resolve={data.leagues}
          >
            { (leagues) => (
              <Form.Select name="leagueId" >
                {leagues.map((league) =>
                  <option key={league.id} value={league.id} >{league.name}</option>)}
              </Form.Select>
            )}
          </Await>
        </React.Suspense>
        <br/>
        <span>Fecha de inicio</span>
        <Form.Input
          type="date"
          name="startDate"
          placeholder="Fecha de inicio"
        />
        <br/>
        <span>Fecha de Fin</span>
        <Form.Input
          type="date"
          name="endDate"
          placeholder="Fecha de Fin"
        />
        <br/>
        <Form.Button type="submit">Guardar</Form.Button>
        <Form.Button type="button">Cancelar</Form.Button>
      </Form>
      <React.Suspense fallback={<p className="bg-white align-middle">Cargando Grupos...</p>}>
        <Await
          resolve={data.groups}
        >
          <ListCompetitionGroup/>
        </Await>
      </React.Suspense>
      
    </>
  )
}

export function ListCompetitionGroup() {
  const data = useAsyncValue()
  return(
    <>
      <Table>
        <Table.THead>
          <Table.TRow>
            <Table.THCol>Nombre</Table.THCol>
            <Table.THCol>Liga</Table.THCol>
            <Table.THCol>Activo</Table.THCol>
            <Table.THCol>Fecha Inicio</Table.THCol>
            <Table.THCol>Fecha Fin</Table.THCol>
          </Table.TRow>
        </Table.THead>
        <tbody>
          { data.map(group => 
            <Table.TRow key={group.id} >
              <Table.Td  className='align-middle'>{group.name}</Table.Td>
              <Table.Td className='align-middle'>{group.leagueId} - {group.leagueId.name}</Table.Td>
              <Table.Td  className='align-middle'>{group.active ? 'Sí' : 'No'}</Table.Td>
              <Table.Td  className='align-middle'>{group.startDate.split('T')[0]}</Table.Td>
              <Table.Td  className='align-middle'>{group.endDate.split('T')[0]}</Table.Td>
            </Table.TRow>
          )
          }
        </tbody>
      </Table>
    </>
  )
}
