export const apiUrl = import.meta.env.PROD
  ? import.meta.env.VITE_API_URL_PROD
  : import.meta.env.VITE_API_URL_DEV
export const serverEndpoints = {
  court: 'basketball-court/',
  available_courts: 'basketball-court/available',
  competition: 'competition/',
  competition_group: 'competition-group/',
  competition_team: 'competition-team/',
  competition_teams: 'competition-team/get-teams/',
  game: 'game/',
  game_create: 'game/create',
  game_result: 'game/addResult/',
  game_feeback: 'game/addFeedback/',
  league: 'league/',
  team: 'team/',
  team_pending_games: 'pending-games/',
  user: 'user/',
  login: 'login',
}
