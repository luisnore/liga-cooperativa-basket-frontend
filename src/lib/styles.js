import { cva } from 'class-variance-authority'

export const buttonVariants = cva(
  'cursor-pointer text-center no-underline py-2 px-4 inline-block rounded-sm border-solid border-2 font-semibold focus:ring-offset-2 focus:outline-none focus:ring-offset-2 transition-all',
  {
    variants: {
      variant: {
        default:
          'border-orange-300 text-orange-500 hover:bg-orange-100 focus:ring-2 focus:ring-orange-300 ',
        subtle:
          'border-emerald-300 text-emerald-500 hover:bg-emerald-100 focus:ring-emerald-300',
      },
    },
    defaultVariants: {
      variant: 'default',
    },
  },
)
