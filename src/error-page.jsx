import React from 'react'
import { useRouteError, isRouteErrorResponse } from 'react-router-dom'
import { Login, Register } from '@/pages'

export default function ErrorPage() {
  const error = useRouteError()
  console.error('ErrorPage:', error)

  if (error.status === 401) {
    return (
      <>
        <h3 className='bg-white' style={{ marginLeft: '20%', color: 'red' }}>
          Login failed. Try again.
        </h3>
        <Login />
      </>
    )
  } else if (error.status === 409) {
    return (
      <>
        <div className='bg-white' style={{ marginLeft: '20%' }}>
          <h1>Oops!</h1>
          <h2>{error.status}</h2>
          <p>{error.statusText}</p>
          {error.data?.message && (
            <p>
              {error.data.message.name}: {error.data.message.message}
            </p>
          )}
        </div>
        <Register />
      </>
    )
  }

  if (isRouteErrorResponse(error)) {
    return (
      <>
        <div className='bg-white'>
          <div style={{ marginLeft: '20%' }}>
            <h1>Oops!</h1>
            <h2>{error.status}</h2>
            <p>
              Sorry, an unexpected error has occurred. Please report next
              message to lcbdevs@gmail.com
            </p>
            <p>{error.statusText}</p>{' '}
            {error.data?.message && (
              <p>
                {error.data.message.name}: {error.data.message.message}
              </p>
            )}
          </div>
        </div>
      </>
    )
  } else {
    return (
      <>
        <div className='bg-white'>
          <h1 style={{ marginLeft: '20%' }}>Oops. Unknown error ocurred</h1>
        </div>
      </>
    )
  }
}
